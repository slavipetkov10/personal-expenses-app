import 'package:flutter/material.dart';

import '../models/transaction.dart';
import './transaction_item.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTx;

  TransactionList(this.transactions, this.deleteTx);

  // Description how to display single transaction from the transactions list
  // and it is called for each transaction
  @override
  Widget build(BuildContext context) {
    print('build() TransactionList');
    // There is a wrapping container with a height for the returned object
    // in the main
    return transactions.isEmpty
        ? LayoutBuilder(builder: (ctx, constraints) {
            return Column(
              children: <Widget>[
                Text(
                  'No transactions added yet!',
                  style: Theme.of(context).textTheme.headline6,
                ),
                // Adds spacing
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: constraints.maxHeight * 0.6,
                  child: Image.asset('assets/images/waiting.png',
                      // The BoxFit takes the height of the parent and the image
                      // fits in the height of the container
                      fit: BoxFit.cover),
                ),
              ],
            );
          })
        // ListView has infinite height and is always scrollable
        : ListView(
            children: transactions
                .map((tx) => TransactionItem(
                      // Every item gets a ValueKey, added to this direct
                      // child, top most widget of the ListView
                      // ValueKey is unique, because of the tx.id unique value
                      // We need a key when TransactionItem is a StatefulWidget
                      // so that Flutter matches the random circle color to the
                      // correct widget when a TransactionItem is deleted
                      key: ValueKey(tx.id),
                      transaction: tx,
                      deleteTx: deleteTx,
                    ))
                .toList(),
          );
  }
}
/*
ListView.builder(
            // This function is executed the number of times
            // equal to the transactions.length
            itemBuilder: (ctx, index) {
              // This widget is built for every item in the list
              return TransactionItem(
                transaction: transactions[index],
                deleteTx: deleteTx,
              );
            },
            itemCount: transactions.length,
          );
 */
