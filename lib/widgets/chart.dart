import 'package:flutter/material.dart';
import '../models/transaction.dart';
import './chart_bar.dart';
import 'package:intl/intl.dart';

class Chart extends StatelessWidget {
  // The chart need the list of transactions from last week
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions){
    print('Constructor Chart');
  }

  // getters are calculated dynamically
  // returns 7 bars for each week day
  // grouping transactions by week day
  List<Map<String, Object>> get groupedTransactionValues {
    // for every element a function is called with an index
    // A list of Maps is returned
    // When the index is 0 , we deduct 0 days from today and we know that
    // it i today
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );
      var totalSum = 0.0; // Total sum for the weekday
      for (var i = 0; i < recentTransactions.length; i++) {
        // Check if the transaction happened today
        // and if we are in the same year and month
        if (recentTransactions[i].date.day == weekDay.day &&
            recentTransactions[i].date.month == weekDay.month &&
            recentTransactions[i].date.year == weekDay.year) {
          totalSum += recentTransactions[i].amount;
        }
      }

      // print(DateFormat.E().format(weekDay).substring(0,1));
      // print(totalSum);

      //return a map Ex: {'day': 'T', 'amount': 9.99};
      // .E() returns a shortcut for the day
      // amount is the sum of all transactions from this day
      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum,
      };
    }).reversed.toList();
  }

  // get the total spending for a week, the total sum for each day gives
  // the total sum for the week
  double get totalSpending {
    // Change the list to another type
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    print('build() Chart');
    return Card(
        elevation: 6,
        margin: EdgeInsets.all(20),
        // The main row for the chart
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            // Set how the bars should spread out with space between them
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            // Map the list to a list of widgets
            children: groupedTransactionValues.map((data) {
              return Flexible(
                fit: FlexFit.tight,
                child: ChartBar(
                  data['day'],
                  data['amount'],
                  totalSpending == 0.0 ? 0.0 :(data['amount'] as double) / totalSpending,
                ),
              );
            }).toList(),
          ),
        ),
      );
  }
}
