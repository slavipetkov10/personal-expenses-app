import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  // The weekday
  final String label;
  final double spendingAmount;
  final double spendingPctOfTotal; // spending percentage

  // With const constructor the instance of the class will be unchangeable
  const ChartBar(this.label, this.spendingAmount, this.spendingPctOfTotal);

  @override
  Widget build(BuildContext context) {
    print('build() ChartBar');

    // Flutter calls the builder function
    return LayoutBuilder(
      builder: (ctx, constraints) {
        // constraints are applied to the ChartBar widget
        // constraints define how much space a widget may take
        return Column(
          children: <Widget>[
            // Shrinks the content so it fits in the box
            Container(
              height: constraints.maxHeight * 0.15,
              child: FittedBox(
                child: Text('\$${spendingAmount.toStringAsFixed(0)}'),
              ),
            ),
            SizedBox(
              height: constraints.maxHeight * 0.05,
            ),
            // The main bar
            Container(
              // Gives the entire bar 60% from the entire height that it
              // parent may take
              height: constraints.maxHeight * 0.6,
              width: 10,
              // Stack can put elements on top of each other
              child: Stack(
                children: <Widget>[
                  // The bottom-most
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  FractionallySizedBox(
                    heightFactor: spendingPctOfTotal,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: constraints.maxHeight * 0.05,
            ),
            Container(
              height: constraints.maxHeight * 0.15,
              // With FittedBox on a small screen the text is resized to fit
              // in the box
              child: FittedBox(
                child: Text(label),
              ),
            ),
          ],
        );
      },
    );
  }
}
