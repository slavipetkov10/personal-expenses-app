import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';

class TransactionItem extends StatefulWidget {
  const TransactionItem({
    Key key,
    @required this.transaction,
    @required this.deleteTx,
  }) : super(key: key);

  final Transaction transaction;
  final Function deleteTx;

  @override
  _TransactionItemState createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {
  Color _bgColor;// background

  // initState is called before build is called
  @override
  void initState() {
    // Every new List item gets a new random background color for the circle
    const availableColors = [
      Colors.red,
      Colors.black,
      Colors.blue,
      Colors.purple,
    ];

    _bgColor = availableColors[Random().nextInt(4)];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 5,
      ),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: _bgColor,
          radius: 30,
          child: Padding(
            padding: const EdgeInsets.all(6),
            child: FittedBox(
              child: Text('\$${widget.transaction.amount}'),
            ),
          ),
        ),
        title: Text(
          widget.transaction.title,
          style: Theme
              .of(context)
              .textTheme
              .headline6,
        ),
        subtitle: Text(
          DateFormat.yMMMd().format(widget.transaction.date),
        ),
        // Add delete button
        trailing: MediaQuery
            .of(context)
            .size
            .width > 460
            ? FlatButton.icon(
          icon: const Icon(Icons.delete),
          // With const, this Text widget will not be
          // rebuilt and the same
          // object will be used when the build method is
          // invoked, the letters in Text will not change
          label: const Text('Delete'),
          textColor: Theme
              .of(context)
              .errorColor,
          onPressed: () => widget.deleteTx(widget.transaction.id),
        )
            : IconButton(
          icon: const Icon(Icons.delete),
          color: Theme
              .of(context)
              .errorColor,
          // When the anonymous function is executed, in there
          // we have our own function that we call and pass our
          // argument
          onPressed: () => widget.deleteTx(widget.transaction.id),
        ),
      ),
    );
  }
}
