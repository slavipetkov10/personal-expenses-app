import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './widgets/new_transaction.dart';
import './widgets/transaction_list.dart';
import './widgets/chart.dart';
import './models/transaction.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations(
  //   [
  //     DeviceOrientation.portraitUp,
  //     DeviceOrientation.portraitDown,
  //   ],
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of the application.
  // When a build method is called all internal widget's constructors are called
  // and for every widget a new object is created
  // and their build methods will execute
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Expenses',
      theme: ThemeData(
        // Based on a color, but generates shades of it automatically
        primarySwatch: Colors.purple,
        // Alternative color
        accentColor: Colors.amber,
        fontFamily: 'Quicksand',
        // Theme settings for titles in the app except for the app bar
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              // Theme for the button
              button: TextStyle(color: Colors.white),
            ),
        // Assign a theme to the title text elements in the app bar
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // String titleInput;
  // String amountInput;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  final List<Transaction> _userTransactions = [
    // Transaction(
    //   id: 't1',
    //   title: 'New Shoes1',
    //   amount: 69.99,
    //   date: DateTime.now(),
    // ),
    // Transaction(
    //   id: 't2',
    //   title: 'Weekly Groceries',
    //   amount: 16.53,
    //   date: DateTime.now(),
    // ),
  ];
  bool _showChart = false;

  // Add a listener
  @override
  void initState() {
    // Whenever the app lifecycle changes call the didChangeAppLifecycleState
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // Prints states such as inactive,paused, resumed when the app
    // is running in the background and can be seen in the task managet
    print(state);
  }

  @override
  dispose() {
    // Clear all listeners to the app lifecycle
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  List<Transaction> get _recentTransactions {
    // Return a new list with the values that satisfy the function in where()
    // Get only the transactions from the last 7 days
    return _userTransactions.where((tx) {
      return tx.date.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _addNewTransaction(String txTitle, double txAmount, DateTime choseDate) {
    final newTx = Transaction(
      title: txTitle,
      amount: txAmount,
      date: choseDate,
      id: DateTime.now().toString(),
    );

    // Calling setState leads to build being called
    setState(() {
      // This should be reflected to the user interface
      _userTransactions.add(newTx);
    });
  }

  void _startAddNewTransaction(BuildContext ctx) {
    // builder returns the widget that should be inside the showModalBottomSheet
    showModalBottomSheet(
      context: ctx,
      builder: (context) {
        return GestureDetector(
          // Does nothing when clicking on the sheet instead of closing it
          onTap: () {},
          child: NewTransaction(_addNewTransaction),
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  // The transaction has an id which is the current date and the method
  // should receive it in order to delete the correct transaction
  void _deleteTransaction(String id) {
    // setState rebuilds the UI
    setState(() {
      // Remove the transaction with the same id as the received in the method
      _userTransactions.removeWhere((tx) {
        return tx.id == id;
      });
    });
  }

  List<Widget> _buildLandscapeContent(
    MediaQueryData mediaQuery,
    AppBar appBar,
    Widget txListWidget,
  ) {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Show Chart', style: Theme.of(context).textTheme.headline6),
          // adaptive changes the look of the switch based on the platform
          Switch.adaptive(
            activeColor: Theme.of(context).accentColor,
            //// On the switch is reflected what the use chose
            value: _showChart,
            onChanged: (val) {
              //Re trigger the build method and update the UI
              setState(() {
                _showChart = val;
              });
            },
          ),
        ],
      ),
      _showChart
          // Insert the transactions from the last week
          ? Container(
              // Deduct the appBar height from this height and calculate 70%
              // from the adjusted height
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      // Remove the system UI bar height and then split the
                      // available space 40/60
                      mediaQuery.padding.top) *
                  0.7,
              child: Chart(_recentTransactions),
            )
          : txListWidget
    ];
  }

  List<Widget> _buildPortraitContent(
    MediaQueryData mediaQuery,
    AppBar appBar,
    Widget txListWidget,
  ) {
    return [
      Container(
        height: (mediaQuery.size.height -
                appBar.preferredSize.height -
                mediaQuery.padding.top) *
            0.3,
        child: Chart(_recentTransactions),
      ),
      txListWidget
    ];
  }

  @override
  Widget build(BuildContext context) {
    print('build() MyHomePageState');
    final mediaQuery = MediaQuery.of(context);
    // It is recalculated whenever the UI is rebuild
    final isLandscape = mediaQuery.orientation == Orientation.landscape;
    // Store the AppBar widget in a variable
    final PreferredSizeWidget appBar = Platform.isIOS
        ? CupertinoNavigationBar(
            // Set the title
            middle: Text('Personal Expenses'),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  child: Icon(CupertinoIcons.add),
                  onTap: () => _startAddNewTransaction(context),
                )
              ],
            ),
          )
        : AppBar(
            title: Text('Personal Expenses'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () => _startAddNewTransaction(context),
              ),
            ],
          );

    final txListWidget = Container(
      height: (mediaQuery.size.height -
              appBar.preferredSize.height -
              mediaQuery.padding.top) *
          0.7,
      child: TransactionList(_userTransactions, _deleteTransaction),
    );

    final pageBody = SafeArea(
      child: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            if (isLandscape)
              ..._buildLandscapeContent(
                mediaQuery,
                appBar,
                txListWidget,
              ),
            // With ... pull the elements from the returned list and pass them
            // as single element to the children <Widget>
            if (!isLandscape)
              ..._buildPortraitContent(
                mediaQuery,
                appBar,
                txListWidget,
              ),
          ],
        ),
      ),
    );

    return Platform.isIOS
        ? CupertinoPageScaffold(
            child: pageBody,
            navigationBar: appBar,
          )
        : Scaffold(
            appBar: appBar,
            //SingleChildScrollView should be on the body, it
            // makes the entire page scrollable
            body: pageBody,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            // Remove the floating button on IOS
            floatingActionButton: Platform.isIOS
                ? Container()
                : FloatingActionButton(
                    child: Icon(Icons.add), //add
                    onPressed: () => _startAddNewTransaction(context),
                  ),
          );
  }
}
